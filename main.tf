resource "aws_instance" "deploy01" {
  ami           =  var.ami
  instance_type =  var.instance
  key_name      =  "awskey3"

  connection {
    host        = self.public_ip
    type        = "ssh"
    user        = var.ansible_user
    private_key = file(var.private_key)
  }

}


#creates default rules. If rules already exist run gives error notifications
#allow SSH and HTTP
resource "aws_security_group" "morning-ssh-http" {
  name        = "morning-ssh-http"
  description = "allow ssh and http traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}


resource "aws_eip" "ip" {
  vpc = true
  instance = aws_instance.deploy01.id
  depends_on = [aws_instance.deploy01]

  provisioner "local-exec" {
	#local-exec needs deprecated variable style
    #command = "sleep 60; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i aws_eip.ip.public_ip, -u var.ansible_user --private-key var.private_key_aws_path ./master.yml"
	
    command = "sleep 60; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i '${aws_eip.ip.public_ip}', -u '${var.ansible_user}' --private-key '${var.private_key_aws_path}' ./master.yml"
  }

}




