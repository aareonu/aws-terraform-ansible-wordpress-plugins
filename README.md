# AWS EC2 Terraform Ansible WordPress and plugins install

Unattended deploy using Terraform and Ansible for Nginx, MySQL, Wordpress site, installing plugins and generating content. 


## AWS EC2 node
Create AWS account https://eu-west-1.console.aws.amazon.com/. Requires creditcard but is free. Beaware! You will pay for bandwith and 2-3 idle test nodes cost ~10$ per month.

Create AWS EC2 node with Ubuntu, get secret .pem key to login over SSH. Lets call this node1.

Connect to node1 using SSH client eg Putty ubuntu@ec2-23-32-34-52.eu-west-1.compute.amazonaws.com

To use putty convert .pem key to old .ppk format https://stackoverflow.com/questions/3190667/convert-pem-to-ppk-file-format


## Ansible
Install ansible https://www.techrepublic.com/article/how-to-install-ansible-on-ubuntu-server-18-04/

```apt-get update && apt-get upgrade```

```apt-get install ansible```


## AWS CLI
Install AWS CLI https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

Generate AWS driver connection credentials https://console.aws.amazon.com/iam/home?#security_credential

```aws configure```

Insert AWS Access Key ID, Secret Access Key	and region. Region is "eu-west-1", not availability_zone "eu-west-1a"


## SSH-key
Create new SSH-key for node2

```aws ec2 create-key-pair --key-name awskey3 --query 'KeyMaterial' --output text > ~/.ssh/awskey3.pem```

```aws ec2 describe-key-pairs```


## GIT code
Git clone to download THIS GIT CODE

```git clone https://aareonu@bitbucket.org/aareonu/aws-terraform-ansible-wordpress-plugins.git```


## Terraform
Download Terraform https://www.terraform.io/downloads.html

Unzip Terraform https://www.hostingmanual.net/zipping-unzipping-files-unix/

```sudo cp terraform /usr/local/bin```

Potencial error: Terraform changed it's variables style after v.11. Examples in internet give "Warning: Interpolation-only expressions are deprecated" https://stackoverflow.com/questions/59038537/fix-interpolation-only-expressions-are-deprecated-warning-in-terraform

Run Terraform to create new ASW EC2 node

```terraform init```

```terraform validate```

```terraform fmt```

```terraform plan -out deploy01```

```terraform apply "deploy01"```


## Firewall AWS EC2
Allow node2 access in firewall. Open node2 instance from ASW console. Edit Inbound rules and add several rules to allow access fro ports 22, 80, 443  0.0.0.0/0


## SSH connection and WordPress access
Use SSH client to connect to node2 

```ssh -i ~/.ssh/awskey3.pem ubuntu@12.232.24.72```

Potencial error: by default use http:// not https:// as http://52.51.63.6 to connect over browser



