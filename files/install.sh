#!/bin/bash

# Istall wordpress
curl -LO https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
cp ./wordpress/wp-config-sample.php ./wordpress/wp-config.php
sudo sed -i 's/password_here/Dest1nyIsAll/' ./wordpress/wp-config.php
rm -rf /var/www/html
sudo mv ./wordpress /var/www/html
chmod -R 0755 /var/www/html
chown -R www-data:www-data /var/www/html


# Find IP and replace in dump.sql
CURRENT_IP="$(curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//')"
sed -i "s/CURRENT_IP/$CURRENT_IP/g" /home/ubuntu/dump.sql

# MySQL database credentials
sudo mysql -uroot -proot -hlocalhost -e "DROP DATABASE IF EXISTS database_name_here; CREATE DATABASE database_name_here;  CREATE USER 'username_here'@'%' IDENTIFIED BY 'Dest1nyIsAll';  GRANT ALL PRIVILEGES ON *.* TO 'username_here'@'%' WITH GRANT OPTION;  FLUSH PRIVILEGES;"
# import SQL dump
mysql -uroot -proot database_name_here < /home/ubuntu/dump.sql



# Install WP-CLI for WP management
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
# WP plugin install
sudo wp plugin install simple-history --activate --path='/var/www/html/' --allow-root
sudo wp plugin install cloudimage --activate --path='/var/www/html/' --allow-root
sudo wp plugin install g-business-reviews-rating --activate --path='/var/www/html/' --allow-root
# WP automatic content
sudo wp menu create "Bastion" --path='/var/www/html/' --allow-root
sudo wp menu item add-custom bastion "Bastion fort" https://en.wikipedia.org/wiki/Bastion_fort --porcelain --path='/var/www/html/' --allow-root
sudo wp menu location assign bastion primary --path='/var/www/html/' --allow-root
sudo wp post create --post_type=page --post_title='Viking' --post_content='Vikings were Norwegians, Swedes and Danes, men who were usually farmers, traders, blacksmiths, and craftsmen. For various reasons, they took to raiding towns, churches and monasteries. Many of the places they attacked were on the coasts as they were easiest to reach.' --path='/var/www/html/' --allow-root
sudo wp user create ubba ubba@wessex.uk --role=administrator --path='/var/www/html/' --allow-root
sudo wp user update ubba --user_pass='1DanishWarlord' --path='/var/www/html/' --allow-root
# This is reduntant but cool way to update WP DB content. To debug use --dry-run option
sudo wp search-replace 'CURRENT_IP' $CURRENT_IP --path='/var/www/html/' --allow-root

# Rrestart services
service php-fpm restart
service mysql restart
service nginx restart
