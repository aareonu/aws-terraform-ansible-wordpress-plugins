#echo some info

output "public_ip" {
    value = aws_eip.ip.public_ip
}

output "ansible_user" {
    value = var.ansible_user
}
