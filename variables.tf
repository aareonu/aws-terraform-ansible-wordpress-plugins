variable "profile" {
  default = "default"
}

#use region "eu-west-1", not Availability zone "eu-west-1a"
variable "region" {
  default = "eu-west-1"
}

#EC2 VPC type
variable "instance" {
  default = "t2.micro"
}

variable "instance_count" {
  default = "1"
}

variable "private_key_aws_path" {
    type = string
    default = "~/.ssh/awskey3.pem"
}

variable "ansible_user" {
  default = "ubuntu"
}

variable "ami" {
  default = "ami-06fd8a495a537da8b"
}
